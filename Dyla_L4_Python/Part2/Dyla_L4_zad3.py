def main():
    print("Lista 4 - zadanie 3 (Instrucja for) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe naturalną n: "))
    positive_numbers = 0
    negative_numbers_n = 0
    null_numbers = 0

    for i in range(n):
        a = float(input("Podaj liczbe rzeczywistą: "))

        if a > 0:
            positive_numbers += 1

        elif a < 0:
            negative_numbers_n += 1

        else:
            null_numbers += 1

    print("Liczb dodatnich jest: {}".format(positive_numbers))
    print("Liczb ujemnych jest: {}".format(negative_numbers_n))
    print("Liczb zerowych jest: {}".format(null_numbers))


main()
