def main():
    print("Lista 4 - zadanie 5 (Instrucja for) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe naturalna n: "))
    a = float(input("Podaj liczbe rzeczywista: "))

    earlier = a

    for i in range(n - 1):
        a = float(input("Podaj liczbe rzeczywista: "))

        if earlier > 0 and a > 0:
            print("({}, {}), ".format(earlier, a))

        earlier = a


main()

