import math


def a(number_quantity):
    print("Podpunkt A:")

    value = 0

    for i in range(number_quantity):
        number = int(input("Podaj liczbę: "))
        if number > 0 and number % 2 != 0:
            value += 1

    print("Nieparzystych liczb jest: {}".format(value))


def b(number_quantity):
    print("Podpunkt B:")

    value = 0

    for i in range(number_quantity):
        number = int(input("Podaj liczbę: "))
        if number > 0 and number % 3 == 0 and number % 5 == 0:
            value += 1

    print("Liczb podzielnych przez 3 i 5 jest: {}".format(value))


def c(number_quantity):
    print("Podpunkt C:")

    value = 0

    for i in range(number_quantity):
        number = int(input("Podaj liczbę: "))

        for j in range(0, number_quantity, 2):
            if number == (j ** 2):
                value += 1

    print("Liczb jako kwadraty liczb parzystych jest: {}".format(value))


def d(number_quantity):
    print("Podpunkt D:")

    value = 0

    numbers = []
    for i in range(number_quantity):
        numbers.append(int(input("Podaj liczbę: ")))

    for i in range(1, number_quantity - 1):
        if numbers[i] < (numbers[i - 1] + numbers[i + 1]) / 2:
            value += 1

    print("Liczb spełniających warunek: {}".format(value))


def e(number_quantity):
    print("Podpunkt E:")

    value = 0

    for i in range(1, number_quantity + 1):
        number = int(input("Podaj liczbę: "))

        if (2 ** i) < number < math.factorial(i):
            value += 1

    print("Liczb spełniających warunek: {}".format(value))


def f(number_quantity):
    print("Podpunkt F:")

    value = 0

    for i in range(1, number_quantity):
        number = int(input("Podaj liczbę: "))
        if i % 2 != 0 and number % 2 == 0:
            value += 1

    print("Liczb parzystych o indeksach nieparzystych jest: {}".format(value))


def g(number_quantity):
    print("Podpunkt G:")

    value = 0

    for i in range(number_quantity):
        number = int(input("Podaj liczbę: "))

        if number >= 0 and number % 2 != 0:
            value += 1

    print("Liczb nieparzystych i nieujemnych jest: {}".format(value))


def h(number_quantity):
    print("Podpunkt H:")

    value = 0

    for i in range(number_quantity):
        number = int(input("Podaj liczbę: "))

        if math.fabs(number) < (i ** 2):
            value += 1

    print("Liczb spełniających warunek |a| < k ** 2: {}".format(value))


def main():
    print("Lista 4 - zadanie 1 (Instrucja for) [Marcin Dyla]")
    print("---------------------------------\n")

    number_quantity = int(input("Podaj ilosc liczb: "))
    print("")

    a(number_quantity)
    print("")
    b(number_quantity)
    print("")
    c(number_quantity)
    print("")
    d(number_quantity)
    print("")
    e(number_quantity)
    print("")
    f(number_quantity)
    print("")
    g(number_quantity)
    print("")
    h(number_quantity)


main()
