def main():
    print("Lista 4 - zadanie 4 (Instrucja for) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe naturalną n: "))
    a = float(input("Podaj liczbe rzeczywista: "))

    min_number = a
    max_number = a

    for i in range(n - 1):
        a = float(input("Podaj liczbę rzeczywistą: "))

        if a < min_number:
            min_number = a

        if a > max_number:
            max_number = a

    print("Minimalna liczba to: {}, a maksymalna to: {}".format(min_number, max_number))


main()
