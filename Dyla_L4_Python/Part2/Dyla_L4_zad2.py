def main():
    print("Lista 4 - zadanie 2 (Instrucja for) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe naturalną: "))
    value = 0

    for i in range(n):
        a = float(input("Podaj liczbe rzeczywistą: "))
        if a > 0:
            value += a * 2

    print("Podwojona suma tych liczb wynosi: {}".format(value))


main()
