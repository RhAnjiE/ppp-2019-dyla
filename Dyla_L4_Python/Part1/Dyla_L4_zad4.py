def main():
    print("Lista 4 - zadanie 4 [Marcin Dyla]")
    print("---------------------------------\n")

    print("Funkcja liniowa: a * x + b = 0")
    a = int(input("Podaj a: "))
    b = int(input("Podaj b: "))

    if a == 0:
        if b == 0:
            print("Równanie tożsamościowe")
        else:
            print("Równanie sprzeczne")

    else:
        x = -b / a

        print("Pierwiastek wynosi: {}".format(x))


main()
