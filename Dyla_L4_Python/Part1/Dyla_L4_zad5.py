def main():
    print("Lista 4 - zadanie 5 [Marcin Dyla]")
    print("---------------------------------\n")

    first = float(input("Podaj pierwszą liczbę: "))
    second = float(input("Podaj drugą liczbę: "))
    third = float(input("Podaj trzecią liczbę: "))

    max_number = first

    if max_number < second:
        max_number = second

    if max_number < third:
        max_number = third

    min_number = first

    if min_number > second:
        min_number = second

    if min_number > third:
        min_number = third

    print("Najwieksza liczba wynosi: {}".format(max_number))
    print("Najmniejsza liczba wynosi: {}".format(min_number))


main()

