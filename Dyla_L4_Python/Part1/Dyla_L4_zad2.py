def sgn(x):
    value = 0

    if x > 0:
        value = 1
    elif x < 0:
        value = -1

    return value


def main():
    print("Lista 4 - zadanie 2 [Marcin Dyla]")
    print("---------------------------------\n")

    number = float(input("Podaj liczbe: "))

    print("Signum liczby wynosi: {}".format(sgn(number)))


main()
