def max3(first, second, third):
    max_number = first

    if max_number < second:
        max_number = second

    if max_number < third:
        max_number = third

    return max_number


def main():
    print("Lista 4 - zadanie 6 [Marcin Dyla]")
    print("---------------------------------\n")

    first = float(input("Podaj pierwszą liczbę: "))
    second = float(input("Podaj drugą liczbę: "))
    third = float(input("Podaj trzecią liczbę: "))

    print("Najwieksza liczba wynosi: {}".format(max3(first, second, third)))


main()
