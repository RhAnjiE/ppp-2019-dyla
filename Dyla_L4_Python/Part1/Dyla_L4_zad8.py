import math


def poprawne_boki(a, b, c):
    if (a < b + c) and (b < a + c) and (c < a + b):
        return True
    else:
        return False


def pole_trojkata(a, b, c):
    p = float((a + b + c) / 2)
    field = float(math.sqrt(p * (p - a) * (p - b) * (p - c)))  # wzor Herona

    return field


def obwod_trojkata(a, b, c):
    circumference = a + b + c

    return circumference


def main():
    print("Lista 4 - zadanie 8 [Marcin Dyla]")
    print("---------------------------------\n")

    a = float(input("Podaj a: "))
    b = float(input("Podaj b: "))
    c = float(input("Podaj c: "))

    if poprawne_boki(a, b, c):
        print("Obwód wynosi: {}".format(pole_trojkata(a, b, c)))
        print("Pole wynosi: {}".format(obwod_trojkata(a, b, c)))

    else:
        print("To nie są boki trójkąta! Kończę program.")


main()
