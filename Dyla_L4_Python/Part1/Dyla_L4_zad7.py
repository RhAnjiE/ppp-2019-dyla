import math


def main():
    print("Lista 4 - zadanie 7 [Marcin Dyla]")
    print("---------------------------------\n")

    a = float(input("Podaj a: "))
    b = float(input("Podaj b: "))
    c = float(input("Podaj c: "))

    if (a < b + c) and (b < a + c) and (c < a + b):
        circumference = a + b + c

        p = float(circumference / 2)
        field = float(math.sqrt(p * (p - a) * (p - b) * (p - c)))  # wzor Herona

        print("Obwód wynosi: {}".format(circumference))
        print("Pole wynosi: {}".format(field))

    else:
        print("To nie są boki trójkąta! Kończę program.")


main()
