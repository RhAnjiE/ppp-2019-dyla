def main():
    print("Lista 4 - zadanie 3 [Marcin Dyla]")
    print("---------------------------------\n")

    first = float(input("Podaj pierwszą liczbę: "))
    second = float(input("Podaj drugą liczbę: "))

    if second != 0:
        print("Wynik dzielenia to: {}".format(first / second))

    else:
        print("Nie można dzielić przez zero!")


main()
