def main():
    print("Lista 4 - zadanie 1 [Marcin Dyla]")
    print("---------------------------------\n")

    number = float(input("Podaj liczbe: "))

    if number < 0:
        number = -number

    print("Wartość bezwględna wynosi: {}".format(number))


main()
