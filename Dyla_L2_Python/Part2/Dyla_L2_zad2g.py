def main():
    print("Lista 2 - zad 2 (g) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe naturalna n: "))
    value1 = 0
    value2 = 1

    if n <= 0:
        print("Nie podales liczby naturalnej!")

        return 1

    for a in range(1, n + 1):
        given_number = float(input("Podaj kolejne a" + str(a) + ": "))

        value1 += given_number
        value2 *= given_number

    print("Wartosc 1 wynosi: {}".format(value1))
    print("Wartosc 2 wynosi: {}".format(value2))
    print("Suma wartosci wynosi: {}".format(value1 + value2))

    return 0


main()
