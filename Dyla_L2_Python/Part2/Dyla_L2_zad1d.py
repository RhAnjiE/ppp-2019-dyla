def main():
    print("Lista 2 - zad 2 (d) [Marcin Dyla]")
    print("---------------------------------\n")

    a = int(input('podaj A: '))
    b = int(input('podaj B: '))

    value = 0
    offset = (a + 1) % 2

    for i in range(a + offset, b + 1, 2):
        print(i)
        value += i

    print("Wartosc wynosi: {}".format(value))


main()
