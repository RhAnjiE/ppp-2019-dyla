import math


def main():
    print("Lista 2 - zad 2 (c) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe naturalna n: "))
    value = 0

    if n <= 0:
        print("Nie podales liczby naturalnej!")

        return 1

    for a in range(1, n + 1):
        given_number = float(input("Podaj kolejne a" + str(a) + ": "))

        value += math.fabs(given_number)

    print("Wartosc wynosi: {}".format(value))

    return 0


main()
