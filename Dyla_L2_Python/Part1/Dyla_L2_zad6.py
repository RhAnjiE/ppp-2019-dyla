import math


def main():
    radians = float(input("Podaj radiany: "))
    degrees = int((radians * 360) / (math.pi * 2))

    print("Radiany: {}".format(radians))
    print("Stopnie: {}".format(degrees))


main()
