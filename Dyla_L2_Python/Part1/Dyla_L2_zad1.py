import math


def main():
    radius = float(input('Podaj promien: '))
    circumference = 2 * math.pi * radius

    print('Obwod okregu o promieniu {} wynosi {}'.format(radius, circumference))


main()
