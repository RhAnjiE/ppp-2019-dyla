import math


def main():
    side_a = float(input('Podaj bok A: '))
    side_b = float(input('Podaj bok B: '))
    side_c = float(input('Podaj bok C: '))

    p = (side_a + side_b + side_c) / 2
    field = math.sqrt(p * (p - side_a) * (p - side_b) * (p - side_c))

    print("Pole trojkata wynosi {}".format(field))


main()
