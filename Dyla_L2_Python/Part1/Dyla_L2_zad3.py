def main():
    bottom_base = float(input('Podaj dolna podstawe: '))
    upper_base = float(input('Podaj gorna podstawe: '))
    height = float(input('Podaj wysokosc: '))

    field = (bottom_base + upper_base) * height / 2

    print("Obdwod trapezu o podstawie dolnej {}, podstawie gornej {} i wysokosci {} wynosi {}".format(
        bottom_base, upper_base, height, field))


main()

