import math


def main():
    degrees = int(input("Podaj kąt: "))
    radians = (degrees * 2 * math.pi) / 360

    print("Stopnie: {}".format(degrees))
    print("Radiany: {}".format(radians))


main()
