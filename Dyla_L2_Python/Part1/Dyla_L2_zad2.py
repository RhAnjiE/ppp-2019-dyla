import math


def main():
    radius = float(input('Podaj promien: '))
    field = math.pi * (radius ** 2)

    print('Pole kola o promieniu {} wynosi {}'.format(radius, field))


main()
