def main():
    print("Lista 7 - zadanie 1.2 [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe calkowita: "))

    for i in range(0, n + 1):
        if i % 3 == 0 or i % 6 == 0:
            continue

        print(i)


main()