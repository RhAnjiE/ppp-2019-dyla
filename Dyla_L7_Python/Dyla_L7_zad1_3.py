def main():
    print("Lista 7 - zadanie 1.3 [Marcin Dyla]")
    print("---------------------------------\n")

    lim = float(input("Podaj lim: "))
    sum_value = 0

    while True:
        sum_value += float(input("Podaj liczbe: "))

        if sum_value > lim:
            break

    print(sum_value)


main()