def main():
    print("Lista 7 - zadanie 2.1 [Marcin Dyla]")
    print("---------------------------------\n")

    try:
        n = int(input("Podaj liczbe calkowita: "))

    except ValueError:
        print("To nie jest liczba calkowita!")
        return

    for i in range(0, n + 1):
        if i % 3 == 0 or i % 6 == 0:
            continue

        print(i)


main()