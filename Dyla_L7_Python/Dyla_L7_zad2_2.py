def main():
    print("Lista 7 - zadanie 2.2 [Marcin Dyla]")
    print("---------------------------------\n")

    try:
        lim = float(input("Podaj lim: "))
        sum_value = 0

        while True:
            try:
                number = float(input("Podaj nowa liczbe: "))
                sum_value += number

                if sum_value > lim:
                    print(sum_value)

                    return

            except ValueError:
                print("To nie jest liczba rzeczywista!")

    except ValueError:
        print("To nie jest liczba rzeczywista!")
        return


main()