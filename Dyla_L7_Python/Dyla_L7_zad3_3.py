import math

def main():
    print("Lista 7 - zadanie 3.3 [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe calkowita n: "))
    primes = set()

    for i in range (2, n + 1):
        primes.add(i)

    for prime in primes.copy():
        for i in range(2, (int)(math.sqrt(n) + 1)):
            if (prime != i and prime % i == 0):
                primes.remove(prime)
                break
            
    print(primes)

main()