def main():
    print("Lista 7 - zadanie 3.1 [Marcin Dyla]")
    print("---------------------------------\n")

    set1 = {1, 2, 3}
    set2 = {3, 4, 5}
    set3 = {3, 7, 1, 2}

    #A
    print("A) ", set1 ^ set2)

    #B
    print("B) ", set1 ^ set2 ^ set3)

    #C
    print("C) ", (set1 & set2 - set3) | (set2 & set3 - set1) | (set1 & set3 - set2))

    #D
    numbers = set()
    for i in range(1, 26):
        if i in set1:
            continue

        numbers.add(i)

    print("D) ", numbers)

    #EE
    numbers = set()
    for i in range(1, 26):
        if i in (set1 & set2 & set3):
            continue

        numbers.add(i)

    print("E) ", numbers)

    #F
    numbers = set()
    for i in range(1, 26):
        if i in (set1 | set2 | set3):
            continue

        numbers.add(i)

    print("F) ", numbers)



main()