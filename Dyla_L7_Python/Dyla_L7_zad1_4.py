def main():
    print("Lista 7 - zadanie 1.4 [Marcin Dyla]")
    print("---------------------------------\n")

    lim = float(input("Podaj lim: "))
    sum_value = 0

    while True:
        number = float(input("Podaj liczbe: "))

        if number <= 0:
            continue

        sum_value += number

        if sum_value > lim:
            break

    print(sum_value)


main()