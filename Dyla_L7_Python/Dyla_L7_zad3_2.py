import string

def a(text_one, text_two):
    set1 = set(string.ascii_letters)
    set2 = set(text_one)
    set3 = set(text_two)

    return set1 & (set2 & set3)

def b(text_one, text_two):
    set1 = set(string.ascii_letters)
    set2 = set(text_one)
    set3 = set(text_two)

    return set1 & (set2 | set3)

def c(text_one, text_two):
    set1 = set(string.ascii_letters)
    set2 = set(text_one)
    set3 = set(text_two)

    return set1 ^ (set2 | set3)

def d(text_one, text_two):
    set1 = set(string.ascii_letters)
    set2 = set(text_one)
    set3 = set(text_two)

    return (set2 & set3) - set1

def e(text_one, text_two):
    set1 = set(string.ascii_letters)
    set2 = set(text_one)
    set3 = set(text_two)

    return (set2 | set3) - set1

def main():
    print("Lista 7 - zadanie 3.2 [Marcin Dyla]")
    print("---------------------------------\n")

    text_one = "Hello World!"
    text_two = "Witaj Świecie!"

    print("A) ", a(text_one, text_two))
    print("B) ", b(text_one, text_two))
    print("C) ", c(text_one, text_two))
    print("D) ", d(text_one, text_two))
    print("E) ", e(text_one, text_two))


main()