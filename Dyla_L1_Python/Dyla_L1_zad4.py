def main():
    print("Lista 1 - zadanie 4 [Marcin Dyla]")
    print("---------------------------------\n")

    account_saldo = 1000
    account_saldo += account_saldo * 0.06
    print("Saldo konta po pierwszym roku: {}".format(account_saldo))

    account_saldo += account_saldo * 0.06
    print("Saldo konta po drugim roku:    {}".format(account_saldo))

    account_saldo += account_saldo * 0.06
    print("Saldo konta po trzeci roku:    {}".format(account_saldo))

    return 0


main()
