import turtle


def main():
    screen = turtle.Screen()
    screen.bgcolor("lightblue")
    screen.title("Marcin Dyla - L3 - Zad. 2")
    screen.setup(800, 600)

    turtle_handle = turtle.Turtle()
    turtle_handle.shape("turtle")

    sides_quantity = 5
    length = int(input('Wprowadź dlugość boków: '))

    for i in range(sides_quantity):
        turtle_handle.fd(length)
        turtle_handle.right(144)

    screen.mainloop()


main()

