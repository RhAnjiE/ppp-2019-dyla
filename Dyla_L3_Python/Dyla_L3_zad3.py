import turtle


def main():
    screen = turtle.Screen()
    screen.bgcolor("lightblue")
    screen.title("Marcin Dyla - L3 - Zad. 3")
    screen.setup(800, 600)

    turtle_handle = turtle.Turtle()
    turtle_handle.shape("turtle")

    sides_quantity = 12

    for i in range(sides_quantity):
        turtle_handle.penup()
        turtle_handle.fd(80)
        turtle_handle.pendown()
        turtle_handle.fd(10)
        turtle_handle.penup()
        turtle_handle.fd(10)
        turtle_handle.pendown()
        turtle_handle.penup()
        turtle_handle.fd(10)
        turtle_handle.stamp()
        turtle_handle.penup()
        turtle_handle.backward(110)
        turtle_handle.right(30)

    screen.mainloop()


main()
