def get_prime_numbers_amount_in_range(m, n):
    i = m
    j = 2

    is_first = True
    amount = 0

    while i < n:
        while j < i:
            if i % j == 0:
                is_first = False

                break

            j += 1

        if is_first:
            amount += 1

        i += 1
        j = 2

        is_first = True

    return amount


def main():
    print("Lista 5 - zadanie 9 [Marcin Dyla]")
    print("---------------------------------\n")

    m = int(input("Podaj dodatnia wartosc m = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if m < 0 and n < 0:
        print("Musisz podac liczby dodatnie!")

    else:
        value = get_prime_numbers_amount_in_range(m, n)

        print("Ilosc liczb pierwszych z przedzialu <{}, {}> wynosi {}".format(m, n, value))


main()
