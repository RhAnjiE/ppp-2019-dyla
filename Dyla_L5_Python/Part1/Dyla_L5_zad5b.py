def calculate(n):
    value = 0

    while n > 0:
        value += n % 10

        n /= 10
        n = int(n)

    return value


def main():
    print("Lista 5 - zadanie 5b [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac liczbe dodatnia!")
    else:
        print("Suma cyfr liczby {} wynosi {} ".format(n, calculate(n)))


main()
