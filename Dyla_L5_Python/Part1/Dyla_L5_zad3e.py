import math


def calculate(x, n):
    i = 1
    j = 0

    value = 0

    while i <= n:
        number = math.sin(x)

        while j < i - 1:
            number = math.sin(number)
            j += 1

        value += number

        i += 1
        j = 0

    return value


def main():
    print("Lista 5 - zadanie 3e [Marcin Dyla]")
    print("---------------------------------\n")

    x = float(input("Podaj wartosc x = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac dodatnia wartosc n")
    else:
        print("Wynik wynosi:", calculate(x, n))


main()
