def calculate(n):
    value = 1

    while n > 0:
        value *= 2
        n -= 1

    return value


def main():
    print("Lista 5 - zadanie 1a [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac wartosc dodatnia!")
    else:
        print("Wynik wynosi: {}".format(calculate(n)))


main()
