import math


def calculate(x, n):
    i = 1
    value = 1

    while i <= n:
        value += math.sin(x) ** i

        i += 1

    return value


def main():
    print("Lista 5 - zadanie 3c [Marcin Dyla]")
    print("---------------------------------\n")

    x = float(input("Podaj wartosc x = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac dodatnia wartosc n")
    else:
        print("Wynik wynosi:", calculate(x, n))


main()
