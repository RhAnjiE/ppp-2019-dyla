def calculate(a, n):
    return a ** n


def main():
    print("Lista 5 - zadanie 2a [Marcin Dyla]")
    print("---------------------------------\n")

    a = float(input("Podaj wartosc a = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac wartosc dodatnia!")
    else:
        print("Wynik wynosi: {}".format(calculate(a, n)))


main()
