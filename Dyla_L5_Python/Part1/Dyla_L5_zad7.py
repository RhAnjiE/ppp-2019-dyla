def is_first_number(n):
    i = 2

    while i < n:
        if n % i == 0:
            return False

        i += 1

    return True


def main():
    print("Lista 5 - zadanie 7 [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 2:
        print("Musisz podac liczbe wieksza od 1!")

    else:
        if is_first_number(n):
            print("{} jest liczba pierwsza!".format(n))

        else:
            print("{} nie jest liczba pierwsza!".format(n))


main()
