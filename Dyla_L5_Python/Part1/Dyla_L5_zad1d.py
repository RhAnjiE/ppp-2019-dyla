import math


def calculate(n):
    i = 1
    value = 1 / (1 + math.sin(1))

    while i <= n:
        sum_value = 0
        j = 1
        while j <= n:
            sum_value += math.sin(j)
            j += 1

        value += 1 / sum_value
        i += 1

    return value


def main():
    print("Lista 5 - zadanie 1d [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac wartosc dodatnia!")
    else:
        print("Wynik wynosi: {}".format(calculate(n)))


main()
