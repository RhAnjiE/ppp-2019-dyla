def calculate(m, n):
    value_sum = 0
    i = 0

    while n > 0:
        if n % 10 == 0 and i < m:
            return -1

        if i < m:
            value_sum += n % 10

            i += 1

        else:
            return value_sum

        n /= 10
        n = int(n)

    return -1


def main():
    print("Lista 5 - zadanie 6 [Marcin Dyla]")
    print("---------------------------------\n")

    m = int(input("Podaj dodatnia wartosc m = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    value = calculate(m, n)

    if m < 0 or n < 0:
        print("Musisz podac liczby dodatnie!")

    elif value == -1:
        print("Niepoprawne dane")

    else:
        print("Wynik liczby {} wynosi {}".format(n, value))


main()
