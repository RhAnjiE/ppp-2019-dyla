def calculate(a, n):
    i = 1
    value = 0

    while i <= n * 2:
        value += 1 / a ** n
        i += 1

    return value


def main():
    print("Lista 5 - zadanie 2d [Marcin Dyla]")
    print("---------------------------------\n")

    a = float(input("Podaj wartosc a = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac wartosc dodatnia!")
    else:
        print("Wynik wynosi: {}".format(calculate(a, n)))


main()
