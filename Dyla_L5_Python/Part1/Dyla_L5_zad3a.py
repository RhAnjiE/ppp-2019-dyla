import math


def calculate(x):
    i = 3
    value = x

    while i <= 13:
        if i % 2 == 0:
            value -= x ** i / math.factorial(i)

        else:
            value += x ** i / math.factorial(i)

        i += 2

    return value


def main():
    print("Lista 5 - zadanie 3a [Marcin Dyla]")
    print("---------------------------------\n")

    x = float(input("Podaj wartosc x = "))
    print("Wynik wynosi:", calculate(x))


main()
