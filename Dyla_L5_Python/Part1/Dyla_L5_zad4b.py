def find_higher(a):
    n = 2
    j = 2
    value = 1

    if value > a:
        return 1

    while True:
        value = 1

        while j < n:
            value += 1 / j

            j += 1

        if value > a:
            return n

        n += 1
        j = 2


def main():
    print("Lista 5 - zadanie 4b [Marcin Dyla]")
    print("---------------------------------\n")

    a = float(input("Podaj wartosc a = "))
    print("Najmniejsza liczba n, ktorej wynik da liczbe wieksza od {} wynosi {}".format(a, find_higher(a)))


main()
