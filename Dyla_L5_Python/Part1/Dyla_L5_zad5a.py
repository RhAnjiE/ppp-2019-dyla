def number_length(n):
    ilosc = 0

    if n == 0:
        return 1

    while n > 0:
        n /= 10
        n = int(n)

        ilosc += 1

    return ilosc


def main():
    print("Lista 5 - zadanie 5a [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac liczbe dodatnia!")
    else:
        print("Podana liczba ma {} cyfr ".format(number_length(n)))


main()
