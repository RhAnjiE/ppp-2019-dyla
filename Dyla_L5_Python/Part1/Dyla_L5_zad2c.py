def calculate(a, n):
    i = 1
    value = float(1 / a)

    while i < n:
        value += 1 / (a * (a + i))
        i += 1

    return value


def main():
    print("Lista 5 - zadanie 2c [Marcin Dyla]")
    print("---------------------------------\n")

    a = float(input("Podaj wartosc a = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac wartosc dodatnia!")
    else:
        print("Wynik wynosi: {}".format(calculate(a, n)))


main()
