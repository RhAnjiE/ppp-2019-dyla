def display_all_prime_numbers_in_range(m, n):
    print("Liczby pierwsze z przedzialu <{}, {}>".format(m, n))

    i = m
    j = 2

    is_first = True

    while i < n:
        while j < i:
            if i % j == 0:
                is_first = False

                break

            j += 1

        if is_first:
            print(i, end=", ")

        i += 1
        j = 2

        is_first = True

    return True


def main():
    print("Lista 5 - zadanie 8 [Marcin Dyla]")
    print("---------------------------------\n")

    m = int(input("Podaj dodatnia wartosc m = "))
    n = int(input("Podaj dodatnia wartosc n = "))

    if m < 0 and n < 0:
        print("Musisz podac liczby dodatnie!")

    else:
        display_all_prime_numbers_in_range(m, n)


main()
