import math


def calculate(x):
    i = 2

    upper_value = 1.0
    lower_value = 1.0

    while i <= 64:
        upper_value *= x - i
        lower_value *= x - (i - 1)

        i *= 2

    return upper_value / lower_value


def main():
    print("Lista 5 - zadanie 3b [Marcin Dyla]")
    print("---------------------------------\n")

    x = float(input("Podaj wartosc x = "))
    print("Wynik wynosi:", calculate(x))


main()
