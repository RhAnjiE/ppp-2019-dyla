def calculate(n):
    value = 0

    i = 0
    while i < n:
        a = float(input("Podaj wartosc a = "))

        value += ((-1) ** i) * a
        i += 1

    return value


def main():
    print("Lista 5 - zadanie 1g (część druga) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac liczbe dodatnia!")

    else:
        print("Wynik wynosi {}".format(calculate(n)))


main()
