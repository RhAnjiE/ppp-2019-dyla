def calculate(n):
    value = 0
    second_value = 1

    i = 0
    while i < n:
        a = float(input("Podaj wartosc a = "))

        value += a
        second_value *= a
        i += 1

    return value + second_value  # Nie rozumiem dokladnie tego przykladu


def main():
    print("Lista 5 - zadanie 1f (część druga) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac liczbe dodatnia!")

    else:
        print("Wynik wynosi {}".format(calculate(n)))


main()
