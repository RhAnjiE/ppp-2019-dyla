import math

def calculate(n):
    value = 1

    i = 0
    while i < n:
        a = float(input("Podaj wartosc a = "))

        value *= math.fabs(a)
        i += 1

    return value

def main():
    print("Lista 5 - zadanie 1d (część druga) [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj dodatnia wartosc n = "))

    if n < 0:
        print("Musisz podac liczbe dodatnia!")

    else:
        print("Wynik wynosi {}".format(calculate(n)))


main()
