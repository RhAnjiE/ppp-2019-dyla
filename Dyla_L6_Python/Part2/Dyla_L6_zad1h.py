def main():
    print("numbers 6 - zadanie 1h (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = []

    for i in range(2, 10002):
        numbers.append(i)

    for i in range(2, 101):
        for element in numbers:
            if element % i == 0 and element > i:
                numbers.remove(element)

    print(numbers)


main()

