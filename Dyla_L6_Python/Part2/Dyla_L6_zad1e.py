def minmax(numbers):
    min_number = 999999
    max_number = 0

    for i in numbers:
        if max_number < i:
            max_number = i

        if min_number > i:
            min_number = i

    return min_number, max_number


def main():
    print("numbers 6 - zadanie 1e (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    print(minmax(numbers))


main()

