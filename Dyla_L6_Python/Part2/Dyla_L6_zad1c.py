def ile_ujemnych(numbers):
    amount = 0

    for i in numbers:
        if i < 0:
            amount += 1

    return amount


def main():
    print("numbers 6 - zadanie 1c (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = [-1, 4, 5, 2, -26, -5432, 1234]

    print(ile_ujemnych(numbers))


main()
