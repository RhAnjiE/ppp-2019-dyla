def crossed_sum(numbers):
    value = 0

    i = 0
    for liczba in numbers:
        value += ((-1) ** i) * liczba

        i += 1

    return value


def main():
    print("numbers 6 - zadanie 1f (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = [1, 4, 9, 16, 9, 7, 4, 9, 11]

    print(crossed_sum(numbers))


main()

