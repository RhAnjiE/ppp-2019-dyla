def iloczyn(numbers):
    value = 1

    for i in numbers:
        value *= i

    return value


def main():
    print("numbers 6 - zadanie 1d (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = [-1, 4, 5, 2, -26, -5432, 1234]

    print(iloczyn(numbers))


main()
