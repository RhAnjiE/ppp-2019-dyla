def i(numbers):
    temp = numbers[0]
    numbers[0] = numbers[-1]
    numbers[-1] = temp

    return numbers


def ii(numbers):
    temp = numbers[-1]

    for i in range(len(numbers) - 1, 0, -1):
        numbers[i] = numbers[i - 1]

    numbers[0] = temp

    return numbers


def iii(numbers):
    for i in range(0, len(numbers)):
        if numbers[i] % 2 == 0:
            numbers[i] = 0

    return numbers


def iv(numbers):
    for i in range(1, len(numbers) - 1):
        if numbers[i - 1] > numbers[i + 1]:
            numbers[i] = numbers[i - 1]
        else:
            numbers[i] = numbers[i + 1]

    return numbers


def v(numbers):
    wielkosc = len(numbers)
    indeks_srodek = wielkosc - 1

    if wielkosc % 2 == 0:
        numbers.remove(numbers[int(indeks_srodek / 2 + 1)])

    numbers.remove(numbers[int(indeks_srodek / 2)])

    return numbers


def vi(numbers):
    new_numbers = []

    for i in range(len(numbers) - 1, -1, -1):
        if numbers[i] % 2 == 0:
            new_numbers.insert(0, numbers[i])
            numbers.remove(numbers[i])

    return new_numbers + numbers


def vii(numbers):
    first_max = numbers[0]
    second_max = numbers[0] - 1

    for i in range(1, len(numbers)):
        if numbers[i] > first_max:
            second_max = first_max
            first_max = numbers[i]

        elif numbers[i] > second_max:
            second_max = numbers[i]

    return second_max


def viii(numbers):
    for i in range(0, len(numbers) - 1):
        if numbers[i] > numbers[i + 1]:
            return False

    return True


def ix(numbers):
    for i in range(0, len(numbers) - 1):
        if numbers[i] == numbers[i + 1]:
            return True

    return False


def x(numbers):
    checked = []

    for number in numbers:
        for element in checked:
            if number == element:
                return True

        checked.append(number)

    return False


def main():
    print("numbers 6 - zadanie 1i (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    print("I    )", i(numbers.copy()))
    print("II   )", ii(numbers.copy()))
    print("III  )", iii(numbers.copy()))
    print("IV   )", iv(numbers.copy()))
    print("V    )", v(numbers.copy()))

    print("VI   )", vi(numbers.copy()))
    print("VII  )", vii(numbers.copy()))
    print("VIII )", viii(numbers.copy()))
    print("IX   )", ix(numbers.copy()))
    print("X    )", x(numbers.copy()))


main()

