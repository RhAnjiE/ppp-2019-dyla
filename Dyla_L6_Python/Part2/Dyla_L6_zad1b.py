def main():
    print("numbers 6 - zadanie 1b (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = []

    i = 1
    while i < 11:
        numbers.append(i)
        i += 1

    print(numbers)

    # ii
    numbers = []

    i = 0
    while i < 11:
        numbers.append(i + i)
        i += 1

    print(numbers)

    # iii
    numbers = []

    i = 1
    while i < 11:
        numbers.append(i ** 2)
        i += 1

    print(numbers)

    # iv
    numbers = []

    i = 0
    while i < 10:
        numbers.append(0)
        i += 1

    print(numbers)

    # v
    numbers = []

    i = 0
    while i < 10:
        numbers.append(int(i % 2 != 0))
        i += 1

    print(numbers)

    # vi
    numbers = []

    i = 0
    while i < 2:
        j = 0
        while j < 5:
            numbers.append(j)
            j += 1

        i += 1

    print(numbers)


main()
