def main():
    print("numbers 6 - zadanie 1g (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers = []

    i = 0
    while i < 10:
        n = int(input("Podaj liczbe: "))

        found = False

        for number in numbers:
            if n == number:
                found = True

        if not found:
            numbers.append(n)

            i += 1

    print(numbers)


main()

