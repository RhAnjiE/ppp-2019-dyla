def equals(a, b):
    if len(a) != len(b):
        return False

    for i in range(0, len(a)):
        if a[i] != b[i]:
            return False

    return True


def main():
    print("numbers 6 - zadanie 1j (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    numbers1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    numbers2 = [1, 2, 3, 4, 5, 6, 7, 8, 10]

    numbers3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    print(equals(numbers1, numbers2))
    print(equals(numbers1, numbers3))


main()

