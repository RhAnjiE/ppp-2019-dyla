def main():
    print("numbers 6 - zadanie 1a (druga część) [Marcin Dyla]")
    print("---------------------------------\n")

    # i
    numbers = []

    for i in range(1, 11):
        numbers.append(i)

    print(numbers)

    # ii
    numbers = []

    for i in range(0, 11):
        numbers.append(i + i)

    print(numbers)

    # iii
    numbers = []

    for i in range(1, 11):
        numbers.append(i ** 2)

    print(numbers)

    # iv
    numbers = []

    for i in range(0, 10):
        numbers.append(0)

    print(numbers)

    # v
    numbers = []

    for i in range(0, 10):
        numbers.append(int(i % 2 != 0))

    print(numbers)

    # vi
    numbers = []

    for i in range(0, 2):
        for j in range(0, 5):
            numbers.append(j)

    print(numbers)


main()
