def reverse(text):
    reversed_text = ""
    for i in range(len(text) - 1, -1, -1):
        reversed_text += text[i]

    return reversed_text


def main():
    print("Lista 6 - zadanie 5 [Marcin Dyla]")
    print("---------------------------------\n")

    text = input("Podaj wyraz: ")

    print(reverse(text))


main()
