def main():
    print("Lista 6 - zadanie 1d [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe n: "))

    if n < 0:
        print("Musisz podac liczbe naturalna!")

    first = 0
    second = 1
    next_number = 0
    found = False

    while not found:
        if first + second < n:
            next_number = first + second

            first = second
            second = next_number

        else:
            found = True

    print("Liczba z ciagu Fibonacciego mniejsza od n wynosi:", next_number)


main()
