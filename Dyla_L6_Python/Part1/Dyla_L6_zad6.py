def palindrom(napis):
    reversed_text = ""
    for i in range(len(napis) - 1, -1, -1):
        reversed_text += napis[i]

    return napis == reversed_text


def main():
    print("Lista 6 - zadanie 6 [Marcin Dyla]")
    print("---------------------------------\n")

    text = input("Podaj wyraz: ")

    if palindrom(text):
        print("Wyraz jest palindromem")
    else:
        print("Wyraz nie jest palindromem")


main()
