def remove(text, remove_phrase):
    only_once = True

    value = ""
    i = 0
    while i < len(text):
        if text[i] == remove_phrase[0] and only_once:
            found = True

            for j in range(0, len(remove_phrase)):
                if i + j < len(text) - 1 and text[i + j] != remove_phrase[j]:
                    found = False

            if i + j < len(text) - 1 and found:
                i += len(remove_phrase) - 1
                only_once = False
            else:
                value += text[i]

        else:
            value += text[i]

        i += 1

    return value


def main():
    print("Lista 6 - zadanie 3 [Marcin Dyla]")
    print("---------------------------------\n")

    text = input("Podaj wyraz: ")
    remove_phrase = input("Podaj remove_phrase ciag znakow: ")

    print(remove(text, remove_phrase))


main()
