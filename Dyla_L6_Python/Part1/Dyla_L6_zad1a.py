def main():
    print("Lista 6 - zadanie 1a [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe n: "))

    if n < 0:
        print("Musisz podac liczbe naturalna!")

    for i in range(2, n):
        prime_number = True

        if n % i == 0:
            for j in range(2, i):
                if i % j == 0:
                    prime_number = False

            if prime_number:
                print(i)


main()
