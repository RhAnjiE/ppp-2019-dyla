import math


def main():
    print("Lista 6 - zadanie 2 [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe n: "))

    if n < 0:
        print("Musisz podac liczbe naturalna!")

    sqrt = int(math.sqrt(n))

    for a in range(0, sqrt):
        for b in range(0, sqrt):
            for c in range(0, sqrt):
                for d in range(0, sqrt):
                    if a ** 2 + b ** 2 + c ** 2 + d ** 2 == n:
                        print("{}, {}, {}, {} do potegi 2 zsumowane wynosza n = {}".format(a, b, c, d, n))

                        return

        print("Nie znaleziono liczb a, b, c, ktore do potegi beda rowne n")


main()
