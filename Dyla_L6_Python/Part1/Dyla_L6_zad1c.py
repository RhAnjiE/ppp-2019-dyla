def main():
    print("Lista 6 - zadanie 1c [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe n: "))

    if n < 0:
        print("Musisz podac liczbe naturalna!")

    for i in range(2, n):
        if n % i != 0:
            continue

        is_prime_number = True
        for j in range(2, i):
            if i % j == 0:
                is_prime_number = False

        if is_prime_number:
            print(i)


main()
