def mirror(text):
    reversed_text = ""
    for i in range(len(text) - 1, -1, -1):
        reversed_text += text[i]

    return text + reversed_text


def main():
    print("Lista 6 - zadanie 7 [Marcin Dyla]")
    print("---------------------------------\n")

    text = input("Podaj wyraz: ")

    print("Tekst: {}".format(mirror(text)))


main()
