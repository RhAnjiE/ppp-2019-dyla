def main():
    print("Lista 6 - zadanie 1b [Marcin Dyla]")
    print("---------------------------------\n")

    n = int(input("Podaj liczbe n: "))

    if n < 0:
        print("Musisz podac liczbe naturalna!")

    for i in range(1, n):
        sum_value = 0

        for j in range(1, i):
            if i % j == 0:
                sum_value += j

        print(i, sum_value)
        if i == sum_value:
            print(i)


main()
